const enviar = document.getElementById('enviar');
const form = document.getElementById('form')

const span_cedula        = document.getElementById('span_cedula');
const span_ruc           = document.getElementById('span_ruc');
const span_pwd           = document.getElementById('span_pass');
const span_val           = document.getElementById('span_pass_val');
const span_email         = document.getElementById('span_email');
const span_nombres       = document.getElementById('span_nombre');
const span_apellidos     = document.getElementById('span_apellido');
const span_empresa       = document.getElementById('span_empresa');
const span_phone         = document.getElementById('span_phone');
const span_activity      = document.getElementById('span_activity');
const span_sede          = document.getElementById('span_sede');
const span_empresa_phone = document.getElementById('span_emp_phone');
const span_type          = document.getElementById('span_type');
const span_pais          = document.getElementById('span_pais');
const span_provincia     = document.getElementById('span_provincia');
const span_ciudad        = document.getElementById('span_ciudad');
const span_direccion     = document.getElementById('span_direccion');



function validaciones() {
    const cedula = document.getElementById('cedula').value;
    const ruc = document.getElementById('ruc').value;
    const pwd = document.getElementById('pass').value;
    const val = document.getElementById('pass_val').value;
    const email = document.getElementById('email').value;
    const nombres = document.getElementById('nombres').value;
    const apellidos = document.getElementById('apellidos').value;
    const empresa = document.getElementById('empresa').value;
    const phone = document.getElementById('phone').value;
    const activity = document.getElementById('activity').value;
    const sede = document.getElementById('sede').value;
    const empresa_phone = document.getElementById('empresa_phone').value;
    const type = document.getElementById('type').selectedIndex;
    const pais = document.getElementById('pais').value;
    const provincia = document.getElementById('provincia').value;
    const ciudad = document.getElementById('ciudad').value;
    const direccion = document.getElementById('direccion').value;

    clearSpan();

    if( nombres === null || nombres.length === 0 || !(/^([a-zA-Z]+)\s([a-zA-Z]+)*$/.test(nombres))){
        span_nombre.innerHTML = `<p>Ingrese sus dos nombres</p>`
        span_nombre.style.display = "block";
        return false;
    }
    if( apellidos === null || apellidos.length === 0 || !(/^([a-zA-Z]+)\s([a-zA-Z]+)*$/.test(apellidos))){
        span_apellido.innerHTML = `<p>Ingrese sus dos apellidos</p>`
        span_apellido.style.display = "block";
        return false;
    }
    if( cedula === null || cedula.length !== 10 || /[^0-9]/.test(cedula)){
        span_cedula.innerHTML = `<p>Ingrese un cédula válida</p>`
        span_cedula.style.display = "block";
        return false;
    }
    if(email === null || email.length === 0 || !(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/.test(email))){
        span_email.innerHTML = `<p>Correo no válido</p>`
        span_email.style.display = "block";
        return false;
    }
    if( pwd === null || !(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(pwd))){
        span_pass.innerHTML = `<p>La contraseña debe tener al menos 8 carácteres, una mayúscula y un número</p>`
        span_pass.style.display = "block";
        return false;
    }
    if(val === null || val.length === 0 || pwd!==val){
        span_pass_val.innerHTML = `<p>Las contraseñas no coinciden</p>`
        span_pass_val.style.display = "block";
        return false;
    }
    if( activity === null || activity.length === 0){
        span_activity.innerHTML = `<p>Este campo es obligatorio</p>`
        span_activity.style.display = "block";
        return false;
    }
    if( empresa === null || empresa.length === 0){
        span_empresa.innerHTML = `<p>Este campo es obligatorio</p>`
        span_empresa.style.display = "block";
        return false;
    }
    if( phone === null || phone.length !== 10 || !(/(09)[0-9]+/.test(phone))){
        span_phone.innerHTML = `<p>Ingrese un teléfono válido</p>`
        span_phone.style.display = "block";
        return false;
    }
    if( ruc === null || ruc.length !== 13 || /[^0-9]/.test(ruc) || ruc.substr(-3) != '001' || ruc.substr(0, 10) != cedula.toString()){
        span_ruc.innerHTML = `<p>Ingrese un RUC válido</p>`
        span_ruc.style.display = "block";
        return false;
    }
    if( sede === null || sede.length === 0 || !(/[A-za-z]+/.test(sede))){
        span_sede.innerHTML = `<p>Este campo es obligatorio</p>`
        span_sede.style.display = "block";
        return false;
    }
    if( empresa_phone === null || empresa_phone.length !== 10 || !(/(09)[0-9]+/.test(empresa_phone))){
        span_empresa_phone.innerHTML = `<p>Ingrese un teléfono válido</p>`
        span_empresa_phone.style.display = "block";
        return false;
    }
    if(type == 0){
        span_type.innerHTML = `<p>Debe seleccionar este campo</p>`
        span_type.style.display = "block";
        return false;
    }
    if( pais === null || pais.length === 0 || !(/[A-za-z]+/.test(pais))){
        span_pais.innerHTML = `<p>País no válido</p>`
        span_pais.style.display = "block";
        return false;
    }
    if( provincia === null || provincia.length === 0 || !(/[A-za-z]+/.test(provincia))){
        span_provincia.innerHTML = `<p>Provincia no válida</p>`
        span_provincia.style.display = "block";
        return false;
    }
    if( ciudad === null || ciudad.length === 0 || !(/[A-za-z]+/.test(ciudad))){
        span_ciudad.innerHTML = `<p>Ciudad no válida</p>`
        span_ciudad.style.display = "block";
        return false;
    }
    if( direccion === null || direccion.length === 0){
        span_direccion.innerHTML = `<p>Este campo es obligatorio</p>`
        span_direccion.style.display = "block";
        return false;
    }
    form.submit();
}

function clearSpan(){
span_cedula             .innerHTML = "";
span_ruc                .innerHTML = "";
span_pwd                .innerHTML = "";
span_val                .innerHTML = "";
span_email              .innerHTML = "";
span_nombres            .innerHTML = "";
span_apellidos          .innerHTML = "";
span_empresa            .innerHTML = "";
span_phone              .innerHTML = "";
span_activity           .innerHTML = "";
span_sede               .innerHTML = "";
span_empresa_phone      .innerHTML = "";
span_type               .innerHTML = "";
span_pais               .innerHTML = "";
span_provincia          .innerHTML = "";
span_ciudad             .innerHTML = "";
span_direccion          .innerHTML = "";

span_cedula             .style.display = "none";
span_ruc                .style.display = "none";
span_pwd                .style.display = "none";
span_val                .style.display = "none";
span_email              .style.display = "none";
span_nombres            .style.display = "none";
span_apellidos          .style.display = "none";
span_empresa            .style.display = "none";
span_phone              .style.display = "none";
span_activity           .style.display = "none";
span_sede               .style.display = "none";
span_empresa_phone      .style.display = "none";
span_type               .style.display = "none";
span_pais               .style.display = "none";
span_provincia          .style.display = "none";
span_ciudad             .style.display = "none";
span_direccion          .style.display = "none";
}
enviar.addEventListener('click', validaciones)
